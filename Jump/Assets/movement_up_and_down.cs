﻿using UnityEngine;
using System.Collections;

public class movement_up_and_down : MonoBehaviour {

	public Transform startMarker;
	public Transform endMarker;
	public Transform markerTemplate;
	public float speed = 70.0F;
	private float startTime;
	private float journeyLength;   


	void Start() { 
		startTime = Time.time;
		journeyLength = Vector3.Distance(startMarker.position, endMarker.position); 
	}
	void Update() {



	}

	void FixedUpdate(){

		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength; 
		//	if (Vector3.Distance(transform.position, endMarker.position)<1f) {
		//if (Input.GetMouseButtonDown (0)){
		if(fracJourney > 1){
			//Debug.Log (fracJourney); 
			startTime = Time.time;
			Vector3 placeholder = startMarker.position;
			startMarker.position = endMarker.position;
			endMarker.position = placeholder;
			distCovered = (Time.time - startTime) * speed;
			fracJourney = distCovered / journeyLength; 
		}


		transform.position = Vector3.Lerp (startMarker.position, endMarker.position, fracJourney);


	}
}
