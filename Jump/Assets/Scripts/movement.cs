﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {
	public static int movespeed = 3;
	public float thrust;
	public Rigidbody rb;
	public static int jumpSpeed = 3;
	public Vector3 userDirection = Vector3.right;
	void Start(){
	
		rb = GetComponent<Rigidbody> ();
	}

	public void Update()
	{
		rb.AddForce (transform.forward * thrust);
		transform.Translate(userDirection * movespeed * Time.deltaTime); 

		if(Input.GetKeyDown (KeyCode.Mouse0) )
		{
			float changeCubePosition = transform.localPosition.y + 1;
			Vector3 positionOfCube = new Vector3 (0.0f,changeCubePosition , 0.0f);
			transform.Translate(positionOfCube);
			 
		}
	}
}