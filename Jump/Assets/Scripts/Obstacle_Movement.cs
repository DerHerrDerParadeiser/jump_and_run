﻿using UnityEngine;
using System.Collections;

public class Obstacle_Movement : MonoBehaviour {

	public static int movespeed_obstacle = 3;
	public Vector3 userDirection = Vector3.right;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate (-userDirection * movespeed_obstacle * Time.deltaTime);
	}
}
