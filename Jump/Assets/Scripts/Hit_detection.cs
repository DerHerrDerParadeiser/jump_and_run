﻿using UnityEngine;
using System.Collections;

public class Hit_detection : MonoBehaviour {
	public GameObject wall;
	void start (){
		wall = GameObject.FindGameObjectWithTag ("Wall");
	}
	void OnCollisionEnter( Collision collision ){
 

		Debug.Log( ReturnDirection( collision.gameObject, this.gameObject ) );
		if (ReturnDirection (collision.gameObject, this.gameObject) == HitDirection.Top || 
			ReturnDirection (collision.gameObject, this.gameObject) == HitDirection.None ||
			collision.gameObject.tag == "Wall") {
		
		} else {
			Application.LoadLevel ("Backup");
		}
	}

	private enum HitDirection { None, Top, Bottom, Forward, Back, Left, Right };
	private HitDirection ReturnDirection( GameObject Object, GameObject ObjectHit ){

		HitDirection hitDirection = HitDirection.None;
		RaycastHit MyRayHit;
		Vector3 direction = ( Object.transform.position - ObjectHit.transform.position ).normalized;
		Ray MyRay = new Ray( ObjectHit.transform.position, direction );

		if ( Physics.Raycast( MyRay, out MyRayHit ) ){

			if ( MyRayHit.collider != null ){

				Vector3 MyNormal = MyRayHit.normal;
				MyNormal = MyRayHit.transform.TransformDirection( MyNormal );

				if (MyNormal == MyRayHit.transform.up) { 
					hitDirection = HitDirection.Top; 

				} else if (MyNormal == -MyRayHit.transform.up) {
					hitDirection = HitDirection.Bottom;
				} else if (MyNormal == MyRayHit.transform.forward) {
					hitDirection = HitDirection.Forward;
				} else if (MyNormal == -MyRayHit.transform.forward) {
					hitDirection = HitDirection.Back;
				} else if (MyNormal == MyRayHit.transform.right) {
					hitDirection = HitDirection.Right;
				} else if (MyNormal == -MyRayHit.transform.right) {
					hitDirection = HitDirection.Left;
				}  
			   
		}
		
	}
		return hitDirection;
}
}